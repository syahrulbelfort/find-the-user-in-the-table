let data_siswa = [
  {
    name: 'Syahrul',
    class: 'IAB',
    age: 23,
  },
  {
    name: 'Jilan',
    class: 'Manajemen',
    age: 20,
  },
  {
    name: 'Sella',
    class: 'Komunikasi',
    age: 25,
  },
  {
    name: 'Bagas',
    class: 'Engineering',
    age: 27,
  },
  {
    name: 'Riyadh',
    class: 'Komunikasi',
    age: 25,
  },
  {
    name: 'Aji',
    class: 'Pariwisata',
    age: 25,
  },
];
const genap = [0, 2, 4, 6, 8];
const ganjil = [1, 3, 5, 7, 9];

function setTableRow() {
  data_siswa.forEach((siswa) => {
    $('#table-siswa tbody').append(`<tr><td>${siswa.name}</td><td>${siswa.class}</td><td>${siswa.age}</td></tr>`);
  });
}
setTableRow();

function find(name) {
  $('#table-siswa tbody').empty();
  if (name) {
    const siswa = data_siswa.find(function findName(data) {
      if (data.name === name) {
        return data;
      }
    });

    if (siswa) {
      $('#table-siswa tbody').append(`<tr><td>${siswa.name}</td><td>${siswa.class}</td><td>${siswa.age}</td></tr>`);
    } else {
      $('#table-siswa tbody').append(`<tr><td colspan="3">Siswa <b>${name} </b> tidak ditemukan!<td></tr>`);
    }
  } else {
    setTableRow();
  }
}

$(document).ready(function () {
  $('input').keyup(function () {
    const name = $('input').val();
    find(name);
  });
  $('input#input-bilangan').keyup(function () {
    const number = $('input#input-bilangan').val();
    $('#table-bilangan tbody').empty();
    if (number) {
      if (genap.includes(parseInt(number))) {
        $(`#table-bilangan tbody`).append(`<tr><td colspan="2"><h4><span class="badge bg-success">${number} ini adalah bilangan Genap</span></h4></td></tr>`);
      } else if (ganjil.includes(parseInt(number))) {
        $('#table-bilangan tbody').append(`<tr><td colspan="2"><h4><span class="badge bg-info">${number} ini adalah bilangan Ganjil</span></h4></td></tr>`);
      } else {
        $('#table-bilangan tbody').append(`<tr><td colspan="2"><h4><span class="badge bg-warning">${number} ini tidak masuk kriteria</span></h4></td></tr>`);
      }
    }
  });
});
